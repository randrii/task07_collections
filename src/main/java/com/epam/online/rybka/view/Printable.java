package com.epam.online.rybka.view;

public interface Printable {
    void print();
}
