package com.epam.online.rybka.view;

import com.epam.online.rybka.Main;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    public static Logger logger = LogManager.getLogger(Main.class);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public View() {
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print");
        menu.put("2", "  2 - get");
        menu.put("3", "  3 - put");
        menu.put("4", "  4 - remove");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::action1);
        methodsMenu.put("2", this::action2);
        methodsMenu.put("3", this::action3);
        methodsMenu.put("4", this::action4);
    }

    private void action1() {
        logger.info("Input string key and value to print: %n");
    }

    private void action2() {
        logger.info("Input string key to find: %n");
    }

    private void action3() {
        logger.info("Input string key and value to add: %n");
    }

    private void action4() {
        logger.info("Input string key to remove: %n");
    }

    private void showMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            showMenu();
            System.out.println("Select from menu");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
