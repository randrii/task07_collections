package com.epam.online.rybka.model;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public interface Nodeble {
    int size();

    boolean isEmpty();

    boolean containsKey(Object key);

    boolean containsValue(Object value);

    Object get(Object key);

    abstract Object put(Object key, Object value);

    Object remove(Object key);

    abstract void putAll(Map m);

    void clear();

    Set keySet();

    Collection values();

    Set entrySet();
}
